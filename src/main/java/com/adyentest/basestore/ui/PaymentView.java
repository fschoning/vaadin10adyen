package com.adyentest.basestore.ui;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.JavaScript;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Tag("payment-view")
//@JavaScript("https://checkoutshopper-test.adyen.com/checkoutshopper/assets/js/sdk/checkoutSDK.1.2.1.min.js")
@JavaScript("https://checkoutshopper-test.adyen.com/checkoutshopper/assets/js/sdk/checkoutSDK.1.2.1.js")
@HtmlImport("src/payment-view.html")        // try import a htmp file with the adyen div and js
@Route(value = "payment")
public class PaymentView extends PolymerTemplate<PaymentView.PaymentViewModel> {
    private static Logger log = LoggerFactory.getLogger(PaymentView.class);

    public String setupResponse;

    public PaymentView() {
        log.info("CONSTRUCTOR PaymentView");
        init();
    }

    public void init() {
        UI.getCurrent().getPage().executeJavaScript("console.log('CONSOLE LOG ENTRY FROM SERVER');");
        getModel().setSetupResponse("This text must be the json response from Adyen setup");
    }

    public interface PaymentViewModel extends TemplateModel {
        void setSetupResponse(String setupResponse);
    }
}
